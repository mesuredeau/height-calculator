Pour lancer le programme, il faut exécuter la commande suivante :

    > pip install -r requirements.txt

PyQt n'a pas été inclus dans la liste car son installation via pip peut casser les installations Anaconda.
Si vous n'utilisez pas Anaconda, veuillez exécuter en plus de la commande précédente la commande :

    > pip install pyqt5