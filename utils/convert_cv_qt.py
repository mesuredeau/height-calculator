import cv2
from PyQt5 import QtGui, QtCore

def convert_cv_qt( cv_img):
    """Convert from an opencv image to QPixmap"""
    rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
    h, w, ch = rgb_image.shape
    bytes_per_line = ch * w
    convert_to_qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
    p = convert_to_qt_format.scaled(self.display_width, self.display_height, QtCore.Qt.KeepAspectRatio)
    return QtGui.QPixmap.fromImage(p)