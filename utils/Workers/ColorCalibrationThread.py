##################################################################################
# Calibration colorimétrique pour discriminer le niveau d'eau (couleur de l'eau) et le fond de l'image (couleur du fond)
# Détermination des 6 valeurs: hmin, hmax, smin, smax, vmin et vmax
# en Différé: on agit sur des images déja enregistrées
# Bernard BENET - Naim CHAOUCH    31 Mars 2022
##################################################################################

import cv2
import numpy as np
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QMutexLocker, QMutex



class ColorCalibration(QThread):
    signal_add_frame = pyqtSignal(tuple)
    signal_change_parametters = pyqtSignal(dict)
    signal_result = pyqtSignal(tuple)
    
    signal_process_buffer = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.buffer = []
        self.buffer_mutex = QMutex()
        
        self.is_processing = False
        self.is_processing_mutex = QMutex()

        self.parametters = {
            "min_x": 0,
            "min_y": 0,
            "max_x": 640,
            "max_y": 480,
            "hue_min": 0,
            "hue_max": 179,
            "sat_min": 0,
            "sat_max": 255,
            "val_min": 0,
            "val_max": 255,
            "scale_x": 100,
        }
        self.parametters_mutex = QMutex()

    @pyqtSlot(dict)
    def set_parametters(self, parametters):
        """
        """
        with QMutexLocker(self.parametters_mutex):
            self.parametters = parametters

    def process_frame(self, frame):
        """
        """
        frame_index = frame[1]
        frame = frame[0]
        if frame is not None:

            parametters = {}
            with QMutexLocker(self.parametters_mutex):
                parametters = self.parametters.copy()
            
            min_x = parametters["min_x"]
            min_y = parametters["min_y"]
            max_x = parametters["max_x"]
            max_y = parametters["max_y"]

            crop = frame[min_y:max_y,
                         min_x:max_x]
            
            crop = frame

            # conversion espace couleur espace BGR vers HSV
            hsv = cv2.cvtColor(crop,cv2.COLOR_BGR2HSV)
            hue,sat,val = cv2.split(hsv)     # séparation des 3 plans HSV


            hue_min = parametters["hue_min"]
            hue_max = parametters["hue_max"]
            sat_min = parametters["sat_min"]
            sat_max = parametters["sat_max"]
            val_min = parametters["val_min"]
            val_max = parametters["val_max"]

            # action sur les valeurs de la track bar pour modifier les valeurs hmin hmax ...
            # seuillage dans les 3 plans HSV
            hthresh = cv2.inRange(np.array(hue),np.array(hue_min),np.array(hue_max))
            sthresh = cv2.inRange(np.array(sat),np.array(sat_min),np.array(sat_max))
            vthresh = cv2.inRange(np.array(val),np.array(val_min),np.array(val_max))

            # Combinaison des 3 seuilages, si les 3 seuilages sont vrais alors le pixel est conservé et blanc
            seuillage = cv2.bitwise_and(hthresh,cv2.bitwise_and(sthresh,vthresh))
            
            kernel_size = parametters["kernel_size"]
            iterations = parametters["iterations"]
            kernel = np.ones((kernel_size,kernel_size),np.uint8) #filtre de convolution pixel,pixel
            # Filtrage morphologique (erosion et dilation)    (choisir différentes combinaisons pour obtenir les résultats désirés)
            image_erode = cv2.erode(seuillage,kernel,iterations = iterations)
            image_dilate = cv2.dilate(image_erode,kernel,iterations = iterations)
            image_morpho = image_dilate
            
            return image_morpho,frame,frame_index

    @pyqtSlot(tuple)
    def add_frame(self, frame):
        with QMutexLocker(self.buffer_mutex):
            self.buffer.append(frame)
        
        should_process = False
        with QMutexLocker(self.is_processing_mutex):
            if not self.is_processing:
                should_process = True
        if should_process:
            self.signal_process_buffer.emit()

    @pyqtSlot()
    def process_buffer(self):
        stop = False
        with QMutexLocker(self.is_processing_mutex):
            self.is_processing = True

        while not stop:
            frame = None
            with QMutexLocker(self.buffer_mutex):
                frame = self.buffer.pop(0)
            processed_frames_tuple = self.process_frame(frame)
            self.signal_result.emit(processed_frames_tuple)
            with QMutexLocker(self.buffer_mutex):
                if len(self.buffer) == 0:
                    stop = True
                    with QMutexLocker(self.is_processing_mutex):
                        self.is_processing = False

    def run(self):
        self.signal_add_frame.connect(self.add_frame)
        self.signal_process_buffer.connect(self.process_buffer)
        self.signal_change_parametters.connect(self.set_parametters)
        self.exec_()