﻿##################################################################################
# Mesure de hauteur d'eau sur des images enregistrées 
# Le programme récupère les images enregistrées et réalise des opérations de traitement d'image
# pour identifier le niveau d'eau (une ligne droite finale caractérise le niveau d'eau obtenu)
# le calcul du niveau d'eau peut se faire en différents points de cette droite
# le point milieu de cette droite a été choisi comme référence pour avoir la mesure de la hauteur d'eau
# Bernard BENET - Naim CHAOUCH    31 Mars 2022
##################################################################################

from math import *
import numpy as np
from scipy.stats import linregress
import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QMutexLocker, QMutex
import os.path
import re
import sys
import tarfile
import math
# import matplotlib.pyplot as pltimport time
# import matplotlib.image as mpimg
from PIL import Image
import cv2
import time
import datetime
#from imutils.video import VideoStream
from datetime import datetime
from six.moves import urllib
import sys
import numba
from numba import jit

@jit(nopython=True)
def parse_array_original(edges,crop,finxnouv,finynouv,origy):
    xlist=[]
    ylist=[]
    for j in range(0,finxnouv,3): # on recupere 1 point sur n en x de la courbe 1D, augmente la rapidite du calcul et diminue la resolution de la droite de regression 
        for i in range(0,finynouv):
            if edges[i][j]==255:           # tester si un pt du profil de la courbe 1D est blanc
                xlist.append(j)          # mets les coordonnees des pts trouves dans le tableau
                ylist.append(i)          # ...

                crop[i,j,0]=0          # mise en couleur verte sur l'image d'origine des points trouvés
                crop[i,j,1]=0
                crop[i,j,2]=0
                break
    return xlist,ylist,crop

@jit(nopython=True)
def get_regression_ys(coefficients, xs):
    """
    """
    ys = []
    for x in xs:
        y = 0
        for i, coefficient in enumerate(coefficients):
            y += coefficient * np.power(x,(len(coefficients) - i - 1))
        if y >= 480:
            y = 479
        ys.append(y)
    return ys

class Mesure(QThread):
    #input signals
    signal_add_frame_tuple = pyqtSignal(tuple)
    signal_change_parametters = pyqtSignal(dict)
    signal_toggle_mesure = pyqtSignal()
    
    #output signals
    signal_edges = pyqtSignal(np.ndarray)
    signal_result = pyqtSignal(np.ndarray)
    signal_pixel_height = pyqtSignal(int)
    signal_send_mesure = pyqtSignal(tuple)
    
    #internal signals
    signal_process_buffer = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.buffer = []
        self.buffer_mutex = QMutex()

        self.is_processing = False
        self.is_processing_mutex = QMutex()

        self.get_mesure = False
        self.get_mesure_mutex = QMutex()

        self.parametters = {}
        self.parametters_mutex = QMutex()

    @pyqtSlot(dict)
    def set_parametters(self, parametters):
        """
        """
        with QMutexLocker(self.parametters_mutex):
            self.parametters = parametters

    def toggle_mesure(self):
        """
        """
        with QMutexLocker(self.get_mesure_mutex):
            self.get_mesure = not self.get_mesure

    def process_frame(self,frame_tuple):
        

        image_morpho = frame_tuple[0]
        frame = frame_tuple[1]
        frame_number = frame_tuple[2]
        
        parametters = {}
        with QMutexLocker(self.parametters_mutex):
            parametters = self.parametters.copy()

        origy = parametters["min_y"]
        origx = parametters["min_x"]
        finx = parametters["max_x"]
        finy = parametters["max_y"]


        # seg fault protection
        if origx < 0 or origx > finx:
            origx = 0
        if origy < 0 or origy > finy:
            origy = 0
        if finx < 0 or finx < origx:
            finx = 640
        if finy < 0 or finy < origy:
            finy = 480

        if frame is not None:
            crop = frame[origy:finy,origx:finx]
            origxnouv=0
            origynouv=0
            finxnouv=finx-origx
            finynouv=finy-origy
            ima = image_morpho[origy:finy,origx:finx]
            n_label, label_img, stats, centroids = cv2.connectedComponentsWithStats(ima)
            n=0
            for i in range(n_label):
                x, y, w, h, area = stats[i]
                if area < 3000: 
                    ima[y:y + h, x:x + w] = 0
                else:
                    n=n+1
            Threshold1 = 150
            Threshold2 = 160
            FilterSize = 10
            edges = cv2.Canny(ima, Threshold1, Threshold2, FilterSize)
            self.signal_edges.emit(edges)
            xlist=[]
            ylist=[]
            should_get_mesure = False
            with QMutexLocker(self.get_mesure_mutex):
                should_get_mesure = self.get_mesure
            
            x_cal = parametters["scale_x"]
            y_cal = parametters["scale_y"]

            self.signal_pixel_height.emit(finy-y_cal)
            if x_cal != 0 or y_cal != 0:
                cv2.line(frame,(x_cal,finy),(x_cal,y_cal),(0,255,),5)
            Heau_cm = None
            if should_get_mesure:

                xlist,ylist,crop = parse_array_original(edges,crop,finxnouv,finynouv,origy)

                x_reshape = np.array(xlist).reshape((-1, 1))
                x = np.array(xlist)
                y = np.array(ylist)

                if len(x)>2:
                    degree = parametters["regression_degree"]
                    coeffs = np.polyfit(x,y,degree)
                    x_list = [i for i in range(origxnouv+10,finxnouv-10)]
                    x_list = np.array(x_list)
                    y_list = get_regression_ys(coeffs, x_list)
                    
                    width = 3
                    for y,x in zip(y_list,x_list):
                        y = int(y)
                        x = int(x)
                        if y not in range(origynouv,finynouv):
                            continue
                        if x not in range(origxnouv,finxnouv):
                            continue
                        for i in range(width):
                            crop[int(y-1-i),x,0]=255
                            crop[int(y-1-i),x,1]=0
                            crop[int(y-1-i),x,2]=0

                    xheau=int(finxnouv/2.0)
                    yheau=int(y_list[xheau])
                    
                    delta_y = finy-origy

                    HauteurEau_Pixel=delta_y-yheau

                    coefficient = parametters["coeff"]
                    Heau_cm=HauteurEau_Pixel*coefficient
                    
                    now = datetime.now()
                    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

            font = cv2.FONT_HERSHEY_SIMPLEX        
            cv2.rectangle(frame, (origx, origy),(finx, finy), (0, 0, 255), 5)
            if Heau_cm is not None:
                cv2.putText(frame,"hauteur= "+str("%3.1f"%Heau_cm)+" cm",(200,200),font, 1,(255,0,0),2,cv2.LINE_AA)
                self.signal_send_mesure.emit((frame_number,Heau_cm))

            frame[origy:finy,origx:finx]=crop
            #cv2.imwrite("frame.jpg",frame)
            return frame

    @pyqtSlot(tuple)
    def add_frame(self, frame_tuple):
        with QMutexLocker(self.buffer_mutex):
            self.buffer.append(frame_tuple)
        
        should_process = False
        with QMutexLocker(self.is_processing_mutex):
            if not self.is_processing:
                should_process = True
        if should_process:
            self.signal_process_buffer.emit()
            
    @pyqtSlot()
    def process_buffer(self):
        stop = False
        with QMutexLocker(self.is_processing_mutex):
            self.is_processing = True
        while not stop:
            frame_tuple = None
            with QMutexLocker(self.buffer_mutex):
                frame_tuple = self.buffer.pop(0)
            
            processed_frame = self.process_frame(frame_tuple)

            if processed_frame is not None:
                self.signal_result.emit(processed_frame)

            with QMutexLocker(self.buffer_mutex):
                if len(self.buffer) == 0:
                    stop = True
                    with QMutexLocker(self.is_processing_mutex):
                        self.is_processing = False

    def run(self):
        self.signal_add_frame_tuple.connect(self.add_frame)
        self.signal_process_buffer.connect(self.process_buffer)
        self.signal_change_parametters.connect(self.set_parametters)
        self.signal_toggle_mesure.connect(self.toggle_mesure)
        self.exec_()