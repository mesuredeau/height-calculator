from utils.preview_ui import Ui_Form
from PyQt5 import QtCore, QtGui, QtWidgets
import cv2

class FenetreVideo(QtWidgets.QWidget, Ui_Form):

    def __init__(self, parent=None,signal=None):
        super(FenetreVideo, self).__init__(parent)
        self.setupUi(self)
        placeholder = QtGui.QPixmap(640,480)
        placeholder.fill(QtGui.QColor(255,0,0))
        self.extended_pixmap.setPixmap(placeholder)
        signal.connect(self.update_pixmap)
        self.show()

    def update_pixmap(self, pixmap):
        """Updates the image_label with a new pixmap"""
        if isinstance(pixmap, tuple):
            pixmap = pixmap[0]
        pixmap = self.convert_cv_qt(pixmap)
        self.extended_pixmap.setPixmap(pixmap)

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_qt_format.scaled(640, 480, QtCore.Qt.KeepAspectRatio)
        return QtGui.QPixmap.fromImage(p)