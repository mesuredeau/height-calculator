from tkinter import filedialog
import cv2
import os
import datetime
from utils.video_ui import Ui_MainWindow
from utils.Workers.InputVideoWorker import VideoThread
from utils.Workers.ColorCalibrationThread import ColorCalibration
from utils.Workers.MesureThread import Mesure
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from utils.FenetreVideo import FenetreVideo
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg,NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

class Logger():

    def __init__(self):
        date = datetime.datetime.now().strftime("%m_%d_%Y-%H_%M_%S")
        self.log = False
        self.filename = f"{date}.log"
        s = "Height Calculator v1.3\n\n"
        self.write_to_file(s)

    def info(self,text):
        time = datetime.datetime.now().strftime("%H:%M:%S")
        info = f"[INFO][{time}]: {text}"
        self.write_to_file(info)

    def warn(self,text):
        time = datetime.datetime.now().strftime("%H:%M:%S")
        warn = f"[WARNING][{time}]: {text}"
        self.write_to_file(warn)

    def err(self,text):
        time = datetime.datetime.now().strftime("%H:%M:%S")
        err = f"[ERROR][{time}]: {text}"
        self.write_to_file(err)
    
    def write_to_file(self,text):
        if not self.log: return
        with open(self.filename,'a') as f:
            f.write(text+'\n')



class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class Video(QtWidgets.QMainWindow,Ui_MainWindow):
    def __init__(self, parent=None):
        super(Video, self).__init__(parent)
        self.setupUi(self)
        
        self.logger = Logger()

        self.parametters = {}
        self.create_initial_parametters()

        self.data = {}
        self.is_recording = False

        self.sc = MplCanvas(self, width=5, height=4, dpi=100)
        self.sc.axes.scatter([], [])
        toolbar = NavigationToolbar(self.sc, self)
        self.plot_layout.addWidget(self.sc)
        self.plot_layout.addWidget(toolbar)

        self.display_width = 320
        self.display_height = 240

        #

        self.mesure_thread = Mesure()
        self.mesure_thread.signal_result.connect(self.set_output_frame_pixmap)
        self.mesure_thread.signal_edges.connect(self.set_edges_pixmap)
        self.mesure_thread.signal_pixel_height.connect(self.set_scale_pixel_height_value)
        self.mesure_thread.signal_send_mesure.connect(self.update_plot)

        self.color_calibration_thread = ColorCalibration()
        self.color_calibration_thread.signal_result.connect(self.set_bitwise_hsv_pixmap)

        placeholder = QtGui.QPixmap(self.display_width,self.display_height)
        placeholder.fill(QtGui.QColor(255,0,0))
        
        self.input_image_pixmap.setPixmap(placeholder)
        
        self.output_pixmap.setPixmap(placeholder)
        self.output_pixmap.mousePressEvent = self.show_full_output_preview

        self.bitwise_hsv_pixmap.setPixmap(placeholder)
        self.bitwise_hsv_pixmap.mousePressEvent = self.show_full_hsv_preview
        
        self.edges_pixmap.setPixmap(placeholder)
        self.edges_pixmap.mousePressEvent = self.show_full_canny_preview
        
        self.input_video_thread = None

        self.mesure_thread.start()
        self.color_calibration_thread.start()

        self.connect_events()
        self.update_parametters()

    #################################################
    #
    # Pixmap management
    #
    #################################################

    def set_input_frame_pixmap(self,frame:tuple):
        self.input_image_pixmap.setPixmap(self.convert_cv_qt(frame[0]))
        self.color_calibration_thread.signal_add_frame.emit(frame)

    def set_output_frame_pixmap(self,frame):
        self.output_pixmap.setPixmap(self.convert_cv_qt(frame))
    
    def set_bitwise_hsv_pixmap(self,frame_tuple):
        self.bitwise_hsv_pixmap.setPixmap(self.convert_cv_qt(frame_tuple[0]))
        self.mesure_thread.signal_add_frame_tuple.emit(frame_tuple)

    def set_edges_pixmap(self,frame):
        self.edges_pixmap.setPixmap(self.convert_cv_qt(frame))


    #################################################
    #
    # Video input management
    #
    #################################################


    def start_capture(self):
        self.start_capture_button.setText("Start Capture")
        self.data = {}
        self.input_image_seekbar_slider.setEnabled(False)
        if self.input_video_thread is not None:
            self.input_video_thread.stop_signal.emit()
            self.input_video_thread.wait()
            self.input_video_thread = None
            return
        if self.input_type_combo_box.currentText() == "Video file":
            filepath = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', './',"Video files (*.mp4 *.avi)")[0]
            if filepath is None or filepath == "" or not os.path.isfile(filepath):
                return
            self.video_path = filepath
            self.cap = cv2.VideoCapture(self.video_path)
            frame_count = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
            if frame_count is not None and frame_count > 0:
                self.input_image_seekbar_slider.setMaximum(frame_count)
                self.input_image_frame_spinbox.setMaximum(frame_count)
                self.input_image_seekbar_slider.setEnabled(True)
            self.input_video_thread = VideoThread(self.cap,type="video")
        elif self.input_type_combo_box.currentText() == "Camera":
            self.video_path = None
            self.cap = cv2.VideoCapture(0)
            self.input_video_thread = VideoThread(self.cap,type="camera")
        else:
            return


        self.parametters["video_frame_rate"] = self.cap.get(cv2.CAP_PROP_FPS)
        self.frameskip_spinbox.setValue(int(self.cap.get(cv2.CAP_PROP_FPS)))
        self.update_parametters()

        frame_width = int(self.cap.get(3))
        frame_height = int(self.cap.get(4))

        #TOOD FIX THIS
        self.x_max_slider.setMaximum(frame_width)
        self.y_max_slider.setMaximum(frame_height)
        self.x_min_slider.setMaximum(frame_width)
        self.y_min_slider.setMaximum(frame_height)

        self.x_max_slider.setValue(frame_width)
        self.y_max_slider.setValue(frame_height)
        self.x_max_spinbox.setValue(frame_width)
        self.y_max_spinbox.setValue(frame_height)
        
        self.input_video_thread.change_pixmap_signal.connect(self.set_input_frame_pixmap)
        self.input_video_thread.current_frame_signal.connect(self.set_current_frame_counter)
        self.start_capture_button.setText("Stop Capture")
        self.input_video_thread.start()
        fps = self.parametters["video_frame_rate"]
        if self.mesure_thread.get_mesure:
            self.mesure_thread.signal_toggle_mesure.emit()
        self.logger.info(f"Started new input video thread, type:{self.input_type_combo_box.currentText()}, filepath: {str(self.video_path)}, framerate:{fps}")

    def seekbar_change(self):
        self.change_frame(self.input_image_seekbar_slider.value())

    def frame_spinbox_change(self):
        self.change_frame(self.input_image_frame_spinbox.value())

    def set_current_frame_counter(self,frame_counter):
        #self.input_image_seekbar_slider.setValue(frame_counter)
        self.input_image_frame_spinbox.setValue(frame_counter)
        pass

    def change_frame(self,frame_counter):
        if self.input_video_thread is None:
            return
        self.input_video_thread.go_to_frame_signal.emit(frame_counter)

    def toggle_pause(self):
        if self.input_video_thread is None:
            return
        self.input_video_thread.pause_signal.emit()


    #################################################
    #
    # Parametters management
    #
    #################################################

    def set_scale_pixel_height_value(self, height):
        self.scale_pixel_height_spinbox.setValue(height)
        if height != 0:
            self.calculated_coeff_spinbox.setValue(self.scale_height_cm_spinbox.value()/height)

    def toggle_height_calculation(self):
        self.mesure_thread.signal_toggle_mesure.emit()

    #################################################
    #
    # HSV values management
    #
    #################################################
    def connect_to_hue_events(self):
        self.connect_to_hue_min()
        self.connect_to_hue_max()

    ## Hue min
    def change_hue_min(self,value):
        self.parametters["hue_min"] = value
        self.update_parametters()

    def hue_min_spinbox_change(self):
        self.hue_min_slider.setValue(self.hue_min_spinbox.value())
        self.change_hue_min(self.hue_min_slider.value())

    def hue_min_slider_change(self):
        self.hue_min_spinbox.setValue(self.hue_min_slider.value())
        self.change_hue_min(self.hue_min_slider.value())

    def connect_to_hue_min(self):
        self.hue_min_slider.valueChanged.connect(self.hue_min_slider_change)
        self.hue_min_spinbox.valueChanged.connect(self.hue_min_spinbox_change)

    ## Hue max
    def change_hue_max(self,value):
        self.parametters["hue_max"] = value
        self.update_parametters()

    def hue_max_spinbox_change(self):
        self.hue_max_slider.setValue(self.hue_max_spinbox.value())
        self.change_hue_max(self.hue_max_slider.value())

    def hue_max_slider_change(self):
        self.hue_max_spinbox.setValue(self.hue_max_slider.value())
        self.change_hue_max(self.hue_max_slider.value())

    def connect_to_hue_max(self):
        self.hue_max_slider.valueChanged.connect(self.hue_max_slider_change)
        self.hue_max_spinbox.valueChanged.connect(self.hue_max_spinbox_change)

    #################################################

    def connect_to_saturation_events(self):
        self.connect_to_saturation_min()
        self.connect_to_saturation_max()

    ## Saturation min
    def change_saturation_min(self,value):
        self.parametters["sat_min"] = value
        self.update_parametters()

    def saturation_min_spinbox_change(self):
        self.saturation_min_slider.setValue(self.saturation_min_spinbox.value())
        self.change_saturation_min(self.saturation_min_slider.value())

    def saturation_min_slider_change(self):
        self.saturation_min_spinbox.setValue(self.saturation_min_slider.value())
        self.change_saturation_min(self.saturation_min_slider.value())

    def connect_to_saturation_min(self):
        self.saturation_min_slider.valueChanged.connect(self.saturation_min_slider_change)
        self.saturation_min_spinbox.valueChanged.connect(self.saturation_min_spinbox_change)

    ## Saturation max
    def change_saturation_max(self,value):
        self.parametters["sat_max"] = value
        self.update_parametters()

    def saturation_max_spinbox_change(self):
        self.saturation_max_slider.setValue(self.saturation_max_spinbox.value())
        self.change_saturation_max(self.saturation_max_slider.value())

    def saturation_max_slider_change(self):
        self.saturation_max_spinbox.setValue(self.saturation_max_slider.value())
        self.change_saturation_max(self.saturation_max_slider.value())

    def connect_to_saturation_max(self):
        self.saturation_max_slider.valueChanged.connect(self.saturation_max_slider_change)
        self.saturation_max_spinbox.valueChanged.connect(self.saturation_max_spinbox_change)

    #################################################

    def connect_to_value_events(self):
        self.connect_to_value_min()
        self.connect_to_value_max()

    ## Value min
    def change_value_min(self,value):
        self.parametters["val_min"] = value
        self.update_parametters()

    def value_min_spinbox_change(self):
        self.value_min_slider.setValue(self.value_min_spinbox.value())
        self.change_value_min(self.value_min_slider.value())

    def value_min_slider_change(self):
        self.value_min_spinbox.setValue(self.value_min_slider.value())
        self.change_value_min(self.value_min_slider.value())

    def connect_to_value_min(self):
        self.value_min_slider.valueChanged.connect(self.value_min_slider_change)
        self.value_min_spinbox.valueChanged.connect(self.value_min_spinbox_change)

    ## Value max
    def change_value_max(self,value):
        self.parametters["val_max"] = value
        self.update_parametters()

    def value_max_spinbox_change(self):
        self.value_max_slider.setValue(self.value_max_spinbox.value())
        self.change_value_max(self.value_max_slider.value())

    def value_max_slider_change(self):
        self.value_max_spinbox.setValue(self.value_max_slider.value())
        self.change_value_max(self.value_max_slider.value())

    def connect_to_value_max(self):
        self.value_max_slider.valueChanged.connect(self.value_max_slider_change)
        self.value_max_spinbox.valueChanged.connect(self.value_max_spinbox_change)

    #################################################
    #
    # Crop management
    #
    #################################################

    def connect_to_crop_events(self):
        self.connect_to_x_min()
        self.connect_to_x_max()
        self.connect_to_y_min()
        self.connect_to_y_max()

    #x_min
    def change_x_min(self,value):
        self.parametters["min_x"] = value
        self.update_parametters()
    
    def x_min_spinbox_change(self):
        if self.x_min_spinbox.value() > self.x_max_spinbox.value():
            self.x_min_spinbox.setValue(self.x_max_spinbox.value()-1)
        self.x_min_slider.setValue(self.x_min_spinbox.value())
        self.change_x_min(self.x_min_slider.value())
    
    def x_min_slider_change(self):
        if self.x_min_slider.value() > self.x_max_slider.value():
            self.x_min_slider.setValue(self.x_max_slider.value()-1)
        self.x_min_spinbox.setValue(self.x_min_slider.value())
        self.change_x_min(self.x_min_slider.value())

    def connect_to_x_min(self):
        self.x_min_slider.valueChanged.connect(self.x_min_slider_change)
        self.x_min_spinbox.valueChanged.connect(self.x_min_spinbox_change)
    
    #x_max
    def change_x_max(self,value):
        self.parametters["max_x"] = value
        self.update_parametters()

    def x_max_spinbox_change(self):
        if self.x_max_spinbox.value() < self.x_min_spinbox.value():
            self.x_max_spinbox.setValue(self.x_min_spinbox.value()+1)
        self.x_max_slider.setValue(self.x_max_spinbox.value())
        self.change_x_max(self.x_max_slider.value())
    
    def x_max_slider_change(self):
        if self.x_max_slider.value() < self.x_min_slider.value():
            self.x_max_slider.setValue(self.x_min_slider.value()+1)
        self.x_max_spinbox.setValue(self.x_max_slider.value())
        self.change_x_max(self.x_max_slider.value())
    
    def connect_to_x_max(self):
        self.x_max_slider.valueChanged.connect(self.x_max_slider_change)
        self.x_max_spinbox.valueChanged.connect(self.x_max_spinbox_change)
    
    #y_min
    def change_y_min(self,value):
        self.parametters["min_y"] = value
        self.update_parametters()
    
    def y_min_spinbox_change(self):
        if self.y_min_spinbox.value() > self.y_max_spinbox.value():
            self.y_min_spinbox.setValue(self.y_max_spinbox.value()-1)
        self.y_min_slider.setValue(self.y_min_spinbox.value())
        self.change_y_min(self.y_min_slider.value())
    
    def y_min_slider_change(self):
        if self.y_min_slider.value() > self.y_max_slider.value():
            self.y_min_slider.setValue(self.y_max_slider.value()-1)
        self.y_min_spinbox.setValue(self.y_min_slider.value())
        self.change_y_min(self.y_min_slider.value())
    
    def connect_to_y_min(self):
        self.y_min_slider.valueChanged.connect(self.y_min_slider_change)
        self.y_min_spinbox.valueChanged.connect(self.y_min_spinbox_change)
    
    #y_max
    def change_y_max(self,value):
        self.parametters["max_y"] = value
        self.update_parametters()
    
    def y_max_spinbox_change(self):
        if self.y_max_spinbox.value() < self.y_min_spinbox.value():
            self.y_max_spinbox.setValue(self.y_min_spinbox.value()+1)
        self.y_max_slider.setValue(self.y_max_spinbox.value())
        self.change_y_max(self.y_max_slider.value())
    
    def y_max_slider_change(self):
        if self.y_max_slider.value() < self.y_min_slider.value():
            self.y_max_slider.setValue(self.y_min_slider.value()+1)
        self.y_max_spinbox.setValue(self.y_max_slider.value())
        self.change_y_max(self.y_max_slider.value())
    
    def connect_to_y_max(self):
        self.y_max_slider.valueChanged.connect(self.y_max_slider_change)
        self.y_max_spinbox.valueChanged.connect(self.y_max_spinbox_change)

    #################################################
    #
    # Other functions
    #
    #################################################
    def create_initial_parametters(self):
        self.parametters = {
            "hue_min":0,
            "hue_max":180,
            "sat_min":0,
            "sat_max":255,  
            "val_min":0,
            "val_max":255,
            "min_x":0,
            "max_x":640,
            "min_y":0,
            "max_y":480,
            "fps":1,
            "scale_x":100,
            "scale_y":100,
            "kernel_size": 3,
            "iterations": 2,
            "coeff": 0.5,
            "parametter_frameskip" : 1,
            "regression_degree": 1,
            "video_frame_rate": 1,
        }

    def update_plot(self,data_tuple):
        if self.input_video_thread is not None:
            if self.input_video_thread.pause:
                return
        if not self.is_recording:
            return
        x = data_tuple[0]/self.parametters["video_frame_rate"]
        y = data_tuple[1]
        # Numero de la frame/fps = temps en secondes
        self.data[data_tuple[0]/self.parametters["video_frame_rate"]] = data_tuple[1]
        #self.data[tuple[0]] = tuple[1]
        true_x =x/self.parametters["video_frame_rate"]
        self.logger.info(f"Added Value {y} for x={true_x}, current number of value : {len(self.data)}\nParametters = {str(self.parametters)}")
        x = np.array(list(self.data.keys()))
        y = np.array(list(self.data.values()))
        self.sc.axes.cla()  # Clear the canvas.
        self.sc.axes.scatter(x, y)
        # Trigger the canvas to update and redraw.
        self.sc.draw()


    def connect_events(self):
        self.connect_to_hue_events()
        self.connect_to_saturation_events()
        self.connect_to_value_events()
        self.connect_to_crop_events()
        #Connect to the parametters inputs/buttons
        self.toggle_height_calculation_button.clicked.connect(self.toggle_height_calculation)
        self.fps_spinbox.valueChanged.connect(self.update_parametters)
        self.scale_x_slider.valueChanged.connect(self.update_parametters)
        self.scale_y_slider.valueChanged.connect(self.update_parametters)
        self.kernel_size_spinbox.valueChanged.connect(self.update_parametters)
        self.iterations_spinbox.valueChanged.connect(self.update_parametters)
        self.coeff_spinbox.valueChanged.connect(self.update_parametters)
        self.input_pause_button.clicked.connect(self.toggle_pause)
        self.input_image_seekbar_slider.valueChanged.connect(self.seekbar_change)
        self.start_capture_button.clicked.connect(self.start_capture)
        self.toggle_record_button.clicked.connect(self.toggle_record)
        self.export_button.clicked.connect(self.export_data)
        self.regression_degree_spinbox.valueChanged.connect(self.update_parametters)
        self.frameskip_spinbox.valueChanged.connect(self.update_parametters)
        #TODO Think 10 min about this
        #self.input_image_frame_spinbox.valueChanged.connect(self.frame_spinbox_change)

    def update_parametters(self):
        self.parametters["kernel_size"] = self.kernel_size_spinbox.value()
        self.parametters["iterations"] = self.iterations_spinbox.value()
        self.parametters["scale_x"] = self.scale_x_slider.value()
        self.parametters["scale_y"] = self.scale_y_slider.value()
        self.parametters["coeff"] = self.coeff_spinbox.value()
        self.parametters["regression_degree"] = self.regression_degree_spinbox.value()

        self.color_calibration_thread.signal_change_parametters.emit(self.parametters)
        self.mesure_thread.signal_change_parametters.emit(self.parametters)
        if self.input_video_thread is not None:
            self.input_video_thread.change_fps(self.fps_spinbox.value())
            self.input_video_thread.change_frameskip(self.frameskip_spinbox.value())


    def toggle_record(self):
        self.is_recording = not self.is_recording
        self.toggle_record_button.setText("Stop recording" if self.is_recording else "Start recording")
        self.logger.info("Stop recording" if not self.is_recording else "Start recording")

    def export_data(self):
        string = ""
        string += "time(in s); height (in cm)\n"
        for i in self.data.items():
            string += str(int(i[0])) + ";" + str(round(i[1],2)) + "\n"
        
        filepath = QFileDialog.getSaveFileName(self, 'Save File', 'data.csv', "CSV (*.csv)")[0]
        if filepath != "":
            with open(filepath, 'w') as f:
                f.write(string)

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_qt_format.scaled(self.display_width, self.display_height, QtCore.Qt.KeepAspectRatio)
        return QtGui.QPixmap.fromImage(p)
    

    def show_full_hsv_preview(self,wut):
        self.hsv_preview = FenetreVideo(None,self.color_calibration_thread.signal_result)

    def show_full_canny_preview(self,wut):
        self.canny_preview = FenetreVideo(None,self.mesure_thread.signal_edges)

    def show_full_output_preview(self,wut):
        self.output_preview = FenetreVideo(None,self.mesure_thread.signal_result)